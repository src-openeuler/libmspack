Name:           libmspack
Version:        0.11
Release:        1
Summary:        Library for CAB and related files compression and decompression
License:        LGPLv2.1
URL:            http://www.cabextract.org.uk/libmspack/
Source0:        https://github.com/kyz/libmspack/archive/v%{version}alpha/%{name}-%{version}alpha.tar.gz

BuildRequires:  doxygen gcc autoconf automake libtool

%description
The purpose of libmspack is to provide both compression and decompression of
some loosely related file formats used by Microsoft.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries, header files and documentation
for developing applications that use %{name}.

%package        help
Summary:        Help documents for %{name}
Obsoletes:      %{name}-doc < 0.2

%description    help
The %{name}-help package contains README, TODO, COPYING.LIB, ChangeLog, AUTHORS
and documentation for %{name}.

%prep
%autosetup -n %{name}-%{version}alpha -p1

chmod a-x mspack/mspack.h
autoreconf -i

%build
CFLAGS="%{optflags} -fno-strict-aliasing" \
%configure --disable-silent-rules --disable-static

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'

iconv -f ISO_8859-1 -t utf8 ChangeLog --output Changelog.utf8
touch -r ChangeLog Changelog.utf8
mv Changelog.utf8 ChangeLog

%files
%{_libdir}/%{name}.so.*
%doc COPYING.LIB
%exclude %{_libdir}/libmspack.la

%files devel
%{_includedir}/mspack.h
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc README TODO ChangeLog AUTHORS
%doc doc

%changelog
* Tue Jul 30 2024 dillon chen <dillon.chen@gmail.com> - 0.11-1
- update to 0.11

* Fri Mar 4 2022 panxiaohe <panxh.life@foxmail.com> - 0.10.1-0.1.1
- update version to 0.10.1

* Thu Jan 21 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 0.8-0.1.1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 0.8

* Thu Jan 9 2020 chengquan<chengquan3@huawei.com> - 0.7-0.1.6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove useless patch

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.7-0.1.5
- Fix CVE-2018-18585 and CVE-2018-18586

* Mon Aug 12 2019 zhuguodong <zhuguodong7@huawei.com> - 0.7-0.1.4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: revert openeuler debranding

* Fri Aug 02 2019 liujing<liujing144@huawei.com> - 0.7-0.1.3
- Type:cves
- ID:CVE-2019-1010305
- SUG:restart
- DESC:fix CVE-2019-1010305

* Thu Aug 01 2019 zhuguodong <zhuguodong7@huawei.com> - 0.7-0.1.2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Wed Aug 01 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.7-0.1.1
- Package init
